import Numeric
import Data.Char 
import Data.Set hiding (map)

-- Euler 
-- Name - SagidulinArtur
-- Key -  959127_EzaW8jIlKfX0xkT6QEqMidJHBNV7QMfh
 
task_4 =
  maximum [x | y<-[100..999], z<-[y..999], let x=y*z, let s=show x, s==reverse s]

task_5 = foldr1 lcm [1..20] --16*9*5*7*11*13*17*19

task_6 = (sum [1..100])^2 - sum (map (^2) [1..100])

task_29 = size $ fromList [x^y | x <- [2..100], y <- [2..100]]


showBin = flip (showIntAtBase 2 intToDigit) ""
isPalindrome x = x == reverse x
task_36 = sum [x | x <- [1,3..1000000], isPalindrome (show x), isPalindrome (showBin x)]


digitSum :: Integer -> Int
digitSum = sum . map digitToInt . show
task_56 :: Int
task_56 = maximum $ map digitSum [a^b | a <- [1..100], b <- [1..100]]


task_63 =length[x^y|x<-[1..9],y<-[1..22],y==(length$show$x^y)]


maxRemainder n = 2 * n * ((n-1) `div` 2)
task_120 = sum $ map maxRemainder [3..1000]


addDigit x = [[sum [x !! b !! c | c <- [0..9-a-b]] | b <- [0..9-a]] | a<-[0..9]]
x3 = [[10-a-b | b <- [0..9-a]] | a <- [0..9]]
x20 = iterate addDigit x3 !! 17
task_164 = sum [x20 !! a !! b | a <- [1..9], b <- [0..9-a]]


fusc' 0=(1,0)
fusc' n
    |even n=(a+b, b)
    |odd n=(a,a+b)
    where
    (a,b)=fusc' $n`div`2
fusc =fst.fusc'
task_169=fusc (10^25)