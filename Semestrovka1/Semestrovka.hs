
data Term = Variable String --Может быть переменной
          | Lambda String Term -- лямбдой терм 
          | Apply Term Term  -- применение терма


-- Реализация класса Show для типа Term. Позволяет выводить терм
instance Show Term where
    show (Variable x) = x
    show (Apply term1 term2) = (show term1) ++ " " ++ (show term2)
    show (Lambda var res) = "(\\" ++ var ++ "." ++ (show res) ++ ")" 

-- получаем уникальную строку, которой нет в списке
renameVar :: [String] -> String -> Int -> String
renameVar lst x n | elem (x ++ (show n)) lst = renameVar lst x (n+1)
                       | otherwise = x ++ (show n)

-- переименовывает все переменные, имена которых совпадают с теми., что есть в списке
renameFreeVars :: [String] -> Term -> Term
renameFreeVars lst (Lambda x term) | elem x lst = Lambda x (renameFreeVars (filter (\el -> el /= x) lst) term)
                                        | otherwise = Lambda x (renameFreeVars lst term)
renameFreeVars lst (Apply t1 t2) = Apply (renameFreeVars lst t1) (renameFreeVars lst t2)
renameFreeVars lst (Variable x) | elem x lst = Variable (renameVar lst x 0)
                                     | otherwise = Variable x


-- Функция, заменяющая данную переменную на нужный терм 
setVariable :: String -> [String] -> Term -> Term -> Term

setVariable var lst (Lambda x term) needTerm | x /= var = Lambda x (setVariable var (x:lst) term needTerm)
                                             | otherwise = Lambda x term
setVariable var lst (Apply term1 term2) needTerm = Apply (setVariable var lst term1 needTerm) (setVariable var lst term2 needTerm)
setVariable var lst (Variable x) needTerm | x == var = renameFreeVars lst needTerm
                                          | otherwise = Variable x  

-- функция, делающая один шаг в нормализации терма
eval1 :: Term -> Term
eval1 (Apply (Lambda var res) t2) = setVariable var [] res t2
eval1 (Apply (Apply term1 term2) t2) = Apply (eval1 (Apply term1 term2)) t2
eval1 term = term

id' = Lambda "x" (Variable "x")
termList = [Apply id' (Apply id' (Lambda "z" (Apply id' (Variable "z")))),
                 Apply (Lambda "x" id') (Variable "x"),
                 Apply (Apply (Lambda "x" (Lambda "y" (Apply (Variable "x") (Variable "y")))) (Lambda "z" (Variable "z"))) (Lambda "u" (Lambda "v" (Variable "v")))]

-- функция, делающая максимальное число шагов в нормализации терма
eval :: Term -> Term
eval (Apply (Lambda var res) t2) = eval (setVariable var [] res t2)
eval (Apply (Apply term1 term2) t2) = eval (Apply (eval (Apply term1 term2)) t2)
eval term = term

-- (\y . (\z . z) y) (\z . z) (\u . \v . v)
-- Apply (Lambda 'x' $ Lambda 'y' $ Variable 'x') (Lambda 'y' $ Variable 'y')